import * as stats from './stats.js'
import * as trip from './trip/actions.js'
import * as transaction from './transaction/actions.js'
import * as dashboard from './dashboard/actions.js'

import store from './store.js'

const today = new Date()

const defaultState = {
  startAt: today,
  endAt: today,
  budget: {
    total: 0,
    used: 0,
    resting: 0,
  },
  days: {
    total: 0,
    used: 0,
    resting: 0,
  },
  today: {
    total: 0,
    used: 0,
    resting: 0,
  },
  daily: {
    budget: 0,
    average: 0,
  },
  categories: {},
  transactions: {},
}

export const dispatch = (event, attributes = {}) => {
  const state = store.read() || defaultState
  const newState = reducer({ type: event, attributes: attributes }, state)

  if (state == newState) return

  dashboard.update(newState)

  store.write(newState)
}

const reducer = ({ type, attributes }, state) => {
  switch (type) {
    case 'app.start': {
      const newState = stats.update({ ...defaultState, ...state })
      trip.load(newState)
      transaction.load(newState)
      dashboard.load(newState)

      return newState
    }

    case 'trip.update': {
      const newState = trip.update(state, attributes)

      return stats.update(newState)
    }

    case 'trip.delete': {
      transaction.clear()
      trip.load(defaultState)

      return defaultState
    }

    case 'trip.share': {
      trip.share(state)

      return state
    }

    case 'trip.import': {
      let attrs = { ...attributes }

      if (typeof (attrs.budget) === "number") {
        attrs = { ...attrs, budget: { total: attrs.budget } }
      }

      const newState = stats.update({ ...defaultState, ...attrs })
      trip.importTrip(newState)
      transaction.load(newState)
      dashboard.update(newState)

      return newState
    }

    case 'transaction.new': {
      transaction.startNew()
      return state
    }

    case 'transaction.save': {
      const newState = transaction.save(state, attributes)

      return stats.update(newState)
    }

    case 'transaction.edit': {
      transaction.edit(state, attributes)
      return state
    }

    case 'transaction.remove': {
      const newState = transaction.remove(state, attributes)

      return stats.update(newState)
    }

    default: {
      console.log('unknonw event', type)

      return state
    }
  }
}
