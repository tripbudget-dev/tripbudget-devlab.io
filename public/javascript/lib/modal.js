export const start = () => {
  document
    .querySelectorAll('[data-modal]')
    .forEach(prepareModal)
}

const prepareModal = (modal) => {
  const triggerName = modal.dataset.modal

  prepareOpeningTriggers(modal, triggerName)
  prepareClosingTriggers(modal, triggerName)
}

const prepareOpeningTriggers = (modal, triggerName) => {
  document
    .querySelectorAll(`[data-modal-open=${triggerName}]`)
    .forEach((trigger) =>
      trigger.addEventListener('click', () => modal.showModal())
    )
}

const prepareClosingTriggers = (modal, triggerName) => {
  document
    .querySelectorAll(`[data-modal-close=${triggerName}]`)
    .forEach((trigger) =>
      trigger.addEventListener('click', () => modal.close())
    )
}
