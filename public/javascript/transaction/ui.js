export const ui = {
  new: document.getElementById('transaction-new'),
  modal: document.querySelector('[data-modal="transaction"]'),
  form: document.getElementById('transaction-form'),
  id: document.getElementById('transaction-id'),
  description: document.getElementById('transaction-description'),
  value: document.getElementById('transaction-value'),
  timestamp: document.getElementById('transaction-timestamp'),
  categories: document.getElementsByName('transaction-category'),
  remove: document.getElementById('transaction-delete'),
  transactionList: document.getElementById('transaction-list')
}
