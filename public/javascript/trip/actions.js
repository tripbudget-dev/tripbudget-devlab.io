import { dateFormatter, fixFloat } from '../lib/formatters.js'
import { ui } from './ui.js'

export const load = (state) =>
  updateUI(state)

export const importTrip = (state) => {
  const trip = {
    ...state,
    startAt: new Date(state.startAt),
    endAt: new Date(state.endAt),
  }

  return updateUI(trip)
}

export const share = async (state) => {
  const trip = JSON.stringify(state, null, 2)
  const file = new File([trip], 'trip.json.txt', { type: 'text/plain', });

  try {
    await navigator.share({ files: [file] })
  } catch {
    // ignore when user cancel sharing
  }

  return state
}

export const update = (state, attributes) => {
  if (isNaN(attributes.budget.total) || attributes.budget.total <= 0) {
    return {
      ...state,
      budget: {
        ...state.budget,
        total: attributes.budget.total
      }
    }
  }

  const trip = {
    ...state,
    budget: {
      total: attributes.budget.total
    },
    startAt: attributes.startAt,
    includeStartAt: attributes.includeStartAt,
    endAt: attributes.endAt,
    includeEndAt: attributes.includeEndAt,
  }

  return updateUI(trip)
}

const updateUI = (trip) => {
  ui.budget.value = currencyToInput(trip.budget.total)
  ui.startAt.valueAsDate = trip.startAt
  ui.includeStartAt.checked = trip.includeStartAt
  ui.endAt.valueAsDate = trip.endAt
  ui.includeEndAt.checked = trip.includeEndAt
  ui.period.innerText = period(trip)

  return trip
}

const period = (trip) => {
  if (trip.budget.total <= 0) return ''

  const included = '✓'

  return [
    trip.includeStartAt ? included : '',
    dateFormatter(trip.startAt, true),
    '-',
    trip.includeEndAt ? included : '',
    dateFormatter(trip.endAt, true)
  ].join(' ')
}

const currencyToInput = (value) => {
  if ((value || 0) == 0) return ''

  return fixFloat(value)
}
