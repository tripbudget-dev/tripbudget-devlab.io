import { ensureFloat } from '../lib/formatters.js'
import { ui } from './ui.js'
import { dispatch } from '../reducer.js'

export const startEventListners = () => {
  ui.share.addEventListener('click', dispatchTripShare)

  ui.importFile.addEventListener('change', importTrip)

  ui.form.addEventListener('reset', dispatchTripRemove)

  ui.budget.addEventListener('keyup', dispatchTripUpdate)

  ui.startAt.addEventListener('change', dispatchTripUpdate)
  ui.includeStartAt.addEventListener('change', dispatchTripUpdate)

  ui.endAt.addEventListener('change', dispatchTripUpdate)
  ui.includeEndAt.addEventListener('change', dispatchTripUpdate)
}

const dispatchTripShare = () =>
  dispatch('trip.share')


const dispatchTripRemove = (event) => {
  event.stopPropagation()
  event.preventDefault()

  if (confirm("This cannot be undone! is Are you sure?")) {
    dispatch('trip.delete')
  }
}

const dispatchTripUpdate = () =>
  dispatch('trip.update', {
    budget: {
      total: ensureFloat(ui.budget.value),
    },
    startAt: ui.startAt.valueAsDate,
    includeStartAt: ui.includeStartAt.checked,
    endAt: ui.endAt.valueAsDate,
    includeEndAt: ui.includeEndAt.checked,
  })

const importTrip = (event) => {
  event.stopPropagation()
  event.preventDefault()

  if (!confirm("Are you sure!? This will replace your current trip!")) {
    ui.importFile.value = null
    ui.importModal.close()
    return
  }

  const files = ui.importFile.files
  const reader = new FileReader();

  reader.onload = function() {
    const trip = JSON.parse(reader.result)

    ui.importFile.value = null
    ui.importModal.close()

    dispatch('trip.import', trip)
  }

  reader.readAsText(files[0]);
}
