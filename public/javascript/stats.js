const BEFORE = -1
const CURRENT = 0
const AFTER = 1

export const update = (state) => {
  const today = zeroHour(new Date())
  const tripStatus = getTripStatus(state, today)
  const dailyTotals = Object.values(state.transactions).reduce((result, transaction) => {
    const timestamp = zeroHour(transaction.timestamp)
    result[timestamp] = (result[timestamp] || 0) + transaction.value

    return result
  }, {})

  const trip = [calcDays, calcBudget, calcToday].reduce((result, calc) => {
    return calc(result, {
      today,
      status: tripStatus,
      totals: dailyTotals
    })
  }, {
    ...state,

    days: {
      ...state.days,
      total: daysBetween(
        state.startAt,
        state.endAt,
        state.includeStartAt,
        state.includeEndAt
      )
    },
  })

  return {
    ...trip,
    daily: {
      budget: trip.budget.total / (trip.days.total || 1),
      average: trip.budget.used / (trip.days.used || 1)
    },
    categories: totalByCategory(trip)
  }
}

const getTripStatus = (state, today) => {
  if (zeroHour(state.endAt) < today) return AFTER
  if (zeroHour(state.startAt) < today) return BEFORE

  return CURRENT
}

const calcDays = (trip, opts) => {
  switch (opts.status) {
    case BEFORE:
      return {
        ...trip,
        days: {
          ...trip.days,
          used: 0,
          resting: trip.days.total
        }
      }
    case CURRENT:
      const used = daysBetween(
        trip.startAt,
        opts.today,
        trip.includeStartAt,
        trip.includeEndAt
      )

      return {
        ...trip,
        days: {
          ...trip.days,
          used: used,
          resting: trip.days.total - used
        }
      }
    case AFTER:
      return {
        ...trip,
        days: {
          ...trip.days,
          used: trip.days.total,
          resting: 0
        }
      }
  }
}

const calcBudget = (trip, _opts) => {
  const used = Object.values(trip.transactions).reduce((result, transaction) => {
    return result + transaction.value
  }, 0)

  return {
    ...trip,
    budget: {
      ...trip.budget,
      used: used,
      resting: trip.budget.total - used
    }
  }
}

const calcToday = (trip, opts) => {
  switch (opts.status) {
    case BEFORE:
      return {
        ...trip,
        today: {
          total: 0,
          resting: 0,
          used: 0
        }
      }
    case CURRENT:
      const used = opts.totals[opts.today] || 0

      return {
        ...trip,
        today: {
          ...trip.today,
          total: trip.budget.resting / (trip.days.resting || 1),
          used: used
        }
      }
    case AFTER:
      return {
        ...trip,
        today: {
          total: 0,
          resting: 0,
          used: 0
        }
      }
  }
}

const totalByCategory = (trip) =>
  Object.values(trip.transactions).reduce((result, transaction) => {
    const total = result[transaction.category] || 0
    result[transaction.category] = total + transaction.value

    return result
  }, {})

const daysBetween = (date1, date2, includeDate1 = false, includeDate2 = false) => {
  const zeroDate1 = zeroHour(date1)
  const zeroDate2 = zeroHour(date2)

  const diff = zeroDate2 - zeroDate1

  if (diff <= 0) return 0

  const days = Math.floor(diff / (1000 * 3600 * 24));

  if (includeDate1 && includeDate2) {
    return days + 1
  } else if (includeDate1 || includeDate2) {
    return days
  } else {
    return days - 1
  }
}

const zeroHour = (value, utc = false) => {
  const date = (value instanceof Date) ? value : new Date(value)

  if (utc) {
    return new Date(
      date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate()
    )
  } else {
    return new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate()
    )
  }
}
