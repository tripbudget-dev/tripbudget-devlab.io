const PWAVersion = 'tripbudget-v2'
const assets = [
  '/',
  '/index.html',
  '/stylesheets/application.css',
  '/javascript/application.js',
  '/images/airplane160.png',
  '/images/airplane512.png',
  '/images/android-chrome-192x192.png',
  '/images/android-chrome-512x512.png',
  '/images/icons/gear.svg',
  '/images/icons/import.svg',
  '/images/icons/share.svg',
]

self.addEventListener('install', (event) =>
  event.waitUntil(
    caches.open(PWAVersion).then((cache) =>
      cache.addAll(assets)
    )
  )
)

self.addEventListener('fetch', (event) => {
  event.respondWith((async () => {
    const cache = await caches.open(PWAVersion)

    try {
      const response = await fetch(event.request)
      cache.put(event.request, response.clone())

      return response

    } catch (error) {
      return await cache.match(event.request)
    }
  })())
})
